import pytest
from fameio.source.loader import PathResolver, load_yaml


class Test:
    def test_load_yaml_plain(self):
        expected = {"ToBe": "ThatIs", "OrNot": "TheQuestion"}
        loaded = load_yaml("tests/yaml/simple.yaml")
        assert expected == loaded

    def test_load_yaml_with_simple_include(self):
        expected = {"Is": {"ToBe": "ThatIs", "OrNot": "TheQuestion"}}
        loaded = load_yaml("tests/yaml/simple_include.yaml")
        assert expected == loaded

    def test_load_yaml_with_nested_include(self):
        expected = {
            "ToBe": {"ThatIs": {"Or": "maybe"}, "TheQuestion": {"not": "?"}},
            "OrNot": {"not": "?"},
        }
        loaded = load_yaml("tests/yaml/a.yaml")
        assert expected == loaded

    def test_with_custom_path_resolver(self):
        class CustomPathResolver(PathResolver):
            def __init__(self):
                super().__init__()
                self.last_file_pattern = ""

            def resolve_yaml_imported_file_pattern(self, root_path: str, file_pattern: str):
                self.last_file_pattern = file_pattern
                return super().resolve_yaml_imported_file_pattern(root_path, file_pattern)

        path_resolver = CustomPathResolver()
        load_yaml("tests/yaml/simple_include.yaml", path_resolver)
        assert path_resolver.last_file_pattern.endswith("simple.yaml")

    def test_with_failed_path_resolver(self):
        class BadPathResolver(PathResolver):
            def resolve_yaml_imported_file_pattern(self, root_path: str, file_pattern: str):
                return []

        with pytest.raises(Exception):
            load_yaml("tests/yaml/simple_include.yaml", BadPathResolver())
