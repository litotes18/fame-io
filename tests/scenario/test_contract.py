from typing import List, Optional

import pytest
from fameio.source.scenario import Attribute, Contract, ScenarioException

from tests.utils import assert_exception_contains


class CustomizedContract(Contract):
    def __init__(
        self,
        sender_id: int,
        receiver_id: int,
        product_name: str,
        delivery_interval: int,
        first_delivery_time: int,
        expiration_time: Optional[int] = None,
    ) -> None:
        super().__init__(
            sender_id,
            receiver_id,
            product_name,
            delivery_interval,
            first_delivery_time,
            expiration_time,
        )
        self._notified_count = 0

    def _notify_data_changed(self):
        self._notified_count += 1

    @property
    def notified_count(self) -> int:
        return self._notified_count


class TestContract:
    def test_init_missing_sender(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract.from_dict(
                {
                    "ReceiverId": 1,
                    "ProductName": "ProdA",
                    "FirstDeliveryTime": 0,
                    "DeliveryIntervalInSteps": 1,
                }
            )
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_receiver(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract.from_dict(
                {
                    "SenderId": 0,
                    "ProductName": "ProdA",
                    "FirstDeliveryTime": 0,
                    "DeliveryIntervalInSteps": 1,
                }
            )
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_product(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract.from_dict(
                {
                    "SenderId": 0,
                    "ReceiverId": 1,
                    "FirstDeliveryTime": 0,
                    "DeliveryIntervalInSteps": 1,
                }
            )
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_delivery_time(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract.from_dict(
                {
                    "SenderId": 0,
                    "ReceiverId": 1,
                    "ProductName": "ProdA",
                    "DeliveryIntervalInSteps": 1,
                }
            )
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_delivery_interval(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract.from_dict(
                {
                    "SenderId": 0,
                    "ReceiverId": 1,
                    "ProductName": "ProdA",
                    "FirstDeliveryTime": 0,
                }
            )
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_expiration_time(self):
        contract = Contract.from_dict(
            {
                "SenderId": 0,
                "ReceiverId": 1,
                "ProductName": "ProdA",
                "FirstDeliveryTime": 0,
                "DeliveryIntervalInSteps": 1,
            }
        )
        assert contract.expiration_time is None

    def test_init_with_expiration_time(self):
        contract = Contract.from_dict(
            {
                "SenderId": 0,
                "ReceiverId": 1,
                "ProductName": "ProdA",
                "FirstDeliveryTime": 0,
                "DeliveryIntervalInSteps": 1,
                "ExpirationTime": 2000,
            }
        )
        assert contract.expiration_time == 2000

    def test_split_contract_definitions_one_to_one(self):
        definition = {
            "SenderId": 0,
            "ReceiverId": 1,
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 1

    def test_split_contract_definitions_missing_sender(self):
        definition = {
            "ReceiverId": 1,
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_split_contract_definitions_missing_receiver(self):
        definition = {
            "SenderId": 0,
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_split_contract_definitions_m_to_n(self):
        definition = {
            "SenderId": [0, 1, 2],
            "ReceiverId": [1, 2, 3, 4],
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MULTI_CONTRACT_CORRUPT, e_info)

    def test_split_contract_definitions_one_to_n(self):
        definition = {
            "SenderId": [0, 1, 2],
            "ReceiverId": 5,
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 1, 2], [5, 5, 5], result)

    @staticmethod
    def assert_contract_dicts_match(contracts: List[dict]) -> None:
        """Asserts that for given list of `contracts`, all values except for keys 'senderId' & 'receiverId' match"""
        for index in range(1, len(contracts)):
            for key in [
                Contract._KEY_PRODUCT,
                Contract._KEY_FIRST_DELIVERY,
                Contract._KEY_FIRST_DELIVERY,
                Contract._KEY_INTERVAL,
                Contract._KEY_EXPIRE,
                Contract._KEY_ATTRIBUTES,
            ]:
                if key in contracts[0]:
                    assert contracts[index][key] == contracts[0][key]

    @staticmethod
    def assert_contract_x_to_y(senders: List[int], receivers: List[int], contracts: List[dict]) -> None:
        """ "Asserts that the n-th item in `contracts` matches the n-th of `senders` and `receivers`"""
        for index in range(len(contracts)):
            assert contracts[index][Contract._KEY_SENDER] == senders[index]
            assert contracts[index][Contract._KEY_RECEIVER] == receivers[index]

    def test_split_contract_definitions_n_to_one(self):
        definition = {
            "SenderId": 0,
            "ReceiverId": [5, 6, 7],
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 0, 0], [5, 6, 7], result)

    def test_split_contract_definitions_n_to_n(self):
        definition = {
            "SenderId": [1, 2, 3],
            "ReceiverId": [5, 6, 7],
            "ProductName": "ProdA",
            "FirstDeliveryTime": 0,
            "DeliveryIntervalInSteps": 1,
        }
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([1, 2, 3], [5, 6, 7], result)

    def test_simple_creation(self):
        c = Contract(
            sender_id=10,
            receiver_id=20,
            product_name="ProductName",
            delivery_interval=100,
            first_delivery_time=200,
        )
        assert c.sender_id == 10
        assert c.display_sender_id == "#10"
        assert c.receiver_id == 20
        assert c.display_receiver_id == "#20"
        assert c.product_name == "ProductName"
        assert c.delivery_interval == 100
        assert c.first_delivery_time == 200
        assert c.expiration_time is None
        assert len(c.attributes) == 0

    def test_notify_add_attribute(self):
        a = CustomizedContract(
            sender_id=10,
            receiver_id=20,
            product_name="ProductName",
            delivery_interval=100,
            first_delivery_time=200,
        )
        assert len(a.attributes) == 0
        assert a.notified_count == 0
        a.add_attribute("attr_name", Attribute("attr_full_name", 10))
        assert len(a.attributes) == 1
        assert a.notified_count == 1
