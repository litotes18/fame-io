import unittest

import pytest
from fameio.source.scenario import Agent, Attribute, ScenarioException

from tests.utils import assert_exception_contains


class CustomizedAgent(Agent):
    def __init__(self, agent_id: int, type_name: str) -> None:
        super().__init__(agent_id, type_name)
        self._notified_count = 0

    def _notify_data_changed(self):
        self._notified_count += 1

    @property
    def notified_count(self) -> int:
        return self._notified_count


class TestAgent(unittest.TestCase):
    def test_init_missing_id(self):
        with pytest.raises(ScenarioException) as e_info:
            # noinspection PyTypeChecker
            Agent(agent_id=None, type_name="AgentType")
        assert_exception_contains(Agent._MISSING_ID, e_info)

    def test_init_id_negative(self):
        with pytest.raises(ScenarioException) as e_info:
            Agent(agent_id=-42, type_name="AgentType")
        assert_exception_contains(Agent._MISSING_ID, e_info)

    def test_init_missing_type(self):
        with pytest.raises(ScenarioException) as e_info:
            # noinspection PyTypeChecker
            Agent(agent_id=42, type_name=None)
        assert_exception_contains(Agent._MISSING_TYPE, e_info)

    def test_init_empty_type(self):
        with pytest.raises(ScenarioException) as e_info:
            Agent(agent_id=42, type_name=" ")
        assert_exception_contains(Agent._MISSING_TYPE, e_info)

    def test_simple_creation(self):
        a = Agent(agent_id=42, type_name="AgentType")
        self.assertEqual(a.id, 42)
        self.assertEqual(a.display_id, "#42")
        self.assertEqual(a.type_name, "AgentType")
        self.assertEqual(len(a.attributes), 0)

    def test_notify_add_attribute(self):
        a = CustomizedAgent(agent_id=42, type_name="AgentType")
        self.assertEqual(len(a.attributes), 0)
        self.assertEqual(a.notified_count, 0)
        a.add_attribute("attr_name", Attribute("attr_full_name", 10))
        self.assertEqual(len(a.attributes), 1)
        self.assertIn("attr_name", a.attributes)
        self.assertEqual(a.notified_count, 1)
