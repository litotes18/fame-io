from typing import List, Dict, Union, Tuple

from fameprotobuf.Services_pb2 import Output
from mockito import mock, when

from fameio.source.cli import ResolveOptions
from fameio.source.results.agent_type import AgentType
from fameio.source.results.data_transformer import (
    DataTransformer,
    INDEX,
    DataTransformerIgnore,
    DataTransformerMerge,
    DataTransformerSplit,
)


def make_series(
    agent_type: AgentType, agent_id: int, data: Dict[int, Dict[int, Union[float, List[Tuple[List[str], float]]]]]
) -> Output.Series:
    """Returns creates Series for given class `name` and given `agent_id`"""
    series = Output.Series()
    series.className = agent_type.get_class_name()
    series.agentId = agent_id
    for time, columns in data.items():
        line = series.line.add()
        line.timeStep = time
        for field_id, value in columns.items():
            column = line.column.add()
            column.fieldId = field_id
            if isinstance(value, (int, float)):
                column.value = value
            elif isinstance(value, list):
                for inner in value:
                    entry = column.entry.add()
                    entry.indexValue.extend(inner[0])
                    entry.value = str(inner[1])
    return series


class DummyTransformer(DataTransformer):
    """For testing of base functionality"""

    pass


def mock_type(fields: List[List[str]]) -> AgentType:
    mocked_agent_type = mock(AgentType)
    when(mocked_agent_type).get_class_name().thenReturn("DummyAgentType")
    when(mocked_agent_type).get_simple_column_map().thenReturn({i: n[0] for i, n in enumerate(fields) if len(n) == 1})
    when(mocked_agent_type).get_merged_column_map().thenReturn({i: n[0] for i, n in enumerate(fields)})
    when(mocked_agent_type).get_complex_column_ids().thenReturn(
        set([i for i, names in enumerate(fields) if len(names) > 1])
    )
    when(mocked_agent_type).get_simple_column_mask().thenReturn([len(names) == 1 for names in fields])

    when(mocked_agent_type).get_column_name_for_id(...).thenReturn(None)
    for i, names in enumerate(fields):
        when(mocked_agent_type).get_column_name_for_id(i).thenReturn(names[0])

    for i, names in enumerate(fields):
        when(mocked_agent_type).get_inner_columns(i).thenReturn(tuple(names[1:]))

    return mocked_agent_type


class TestDataTransformer:
    def test_build_returns_class_from_mode(self):
        assert isinstance(DataTransformer.build(ResolveOptions.IGNORE), DataTransformerIgnore)
        assert isinstance(DataTransformer.build(ResolveOptions.MERGE), DataTransformerMerge)
        assert isinstance(DataTransformer.build(ResolveOptions.SPLIT), DataTransformerSplit)

    def test_extract_agent_data_empty_list_returns_empty_dict(self):
        agent_type = mock_type([])
        result = DummyTransformer().extract_agent_data([], agent_type)
        assert len(result) == 0

    def test_extract_agent_data_none_list_returns_empty_dict(self):
        agent_type = mock_type([])
        result = DummyTransformer().extract_agent_data(None, agent_type)  # noqa
        assert len(result) == 0

    def test_extract_agent_data_simple_one_series_one_agent_column_names(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)
        assert "A" in result[None].columns
        assert "B" in result[None].columns

    def test_extract_agent_data_simple_one_series_one_agent_data_match(self):
        column_names = ["A", "B"]
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)[None]
        result_0 = result.xs(0, level=INDEX[0])
        for time, columns in data.items():
            row = result_0.xs(time)
            for column_index, value in columns.items():
                assert row[column_names[column_index]] == value

    def test_extract_agent_data_one_series_one_agent_shape_all_simple(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)
        assert len(result) == 1
        assert result[None].shape[0] == 3
        assert result[None].shape[1] == 2

    def test_extract_agent_data_one_series_one_agent_column_names(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)[None]
        assert "A" in result.columns
        assert "B" in result.columns

    def test_extract_agent_data_one_series_one_agent_data_match(self):
        column_names = ["A", "B"]
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)[None]
        result_0 = result.xs(0, level=INDEX[0])
        for time, columns in data.items():
            row = result_0.xs(time)
            for column_index, value in columns.items():
                assert row[column_names[column_index]] == value

    def test_extract_agent_data_one_series_one_agent_multi_index(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series], agent_type)[None]
        index = result.index
        agents = set(index.get_level_values(0))
        assert len(agents) == 1 and 0 in agents
        times = set(index.get_level_values(1))
        assert len(times) == 3 and 100 in times and 200 in times and 300 in times

    def test_extract_agent_data_many_series_one_agent(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series_one = make_series(agent_type, agent_id=0, data=data)
        data = {1000: {0: 100, 1: 101}, 2000: {0: 200}, 3000: {1: 301}}
        series_two = make_series(agent_type, agent_id=0, data=data)
        result = DummyTransformer().extract_agent_data([series_one, series_two], agent_type)[None]
        times = set(result.index.get_level_values(1))
        assert len(times) == 6
        for time in 100, 200, 300, 1000, 2000, 3000:
            assert time in times

    def test_extract_agent_data_many_series_multiple_agents(self):
        agent_type = mock_type([["A"], ["B"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series_zero = make_series(agent_type, agent_id=0, data=data)
        series_one = make_series(agent_type, agent_id=1, data=data)
        result = DummyTransformer().extract_agent_data([series_zero, series_one], agent_type)[None]
        agents = set(result.index.get_level_values(0))
        assert 0 in agents and 1 in agents


class TestDataTransformerIgnore:
    def test_extract_agent_data_one_series_one_agent_shape_mixed_ignores_complex_type(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerIgnore().extract_agent_data([series], agent_type)
        assert len(result) == 1
        assert result[None].shape[0] == 3
        assert result[None].shape[1] == 2

    def test_extract_agent_data_one_series_one_agent_shape_mixed_ignores_complex_data(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerIgnore().extract_agent_data([series], agent_type)
        assert len(result) == 1
        assert result[None].shape[0] == 3
        assert result[None].shape[1] == 2


class TestDataTransformerMerge:
    def test_extract_agent_data_mixed_complex_no_extra_dataframes(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerMerge().extract_agent_data([series], agent_type)
        assert len(result) == 1

    def test_extract_agent_data_mixed_complex_all_columns_present(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerMerge().extract_agent_data([series], agent_type)[None]
        assert result.shape[0] == 3
        assert result.shape[1] == 3
        assert "A" in result.columns
        assert "B" in result.columns
        assert "Complex" in result.columns

    def test_extract_agent_data_mixed_complex_data_stored(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerMerge().extract_agent_data([series], agent_type)[None]
        result_0 = result.xs(0, level=INDEX[0])
        for time, columns in data.items():
            row = result_0.xs(time)
            for column_index, value in columns.items():
                if column_index == 2 and time == 100:
                    assert str(row[column_index]) == "[(('x1', 'y1'), '1'), (('x2', 'y2'), '4')]"
                else:
                    assert row[column_index] == value


class TestDataTransformerSplit:
    def test_extract_agent_data_mixed_complex_extra_dataframes(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerSplit().extract_agent_data([series], agent_type)
        assert len(result) == 2

    def test_extract_agent_data_mixed_complex_all_columns_split(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        data = {100: {0: 100, 1: 101, 2: [(["x1", "y1"], 1), (["x2", "y2"], 4)]}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerSplit().extract_agent_data([series], agent_type)
        assert "A" in result[None].columns
        assert "B" in result[None].columns
        assert "Complex" in result["Complex"].columns

    def test_extract_agent_data_mixed_complex_data_stored(self):
        agent_type = mock_type([["A"], ["B"], ["Complex", "Sub1", "Sub2"]])
        complex_value = [(["x1", "y1"], 1), (["x2", "y2"], 4)]
        data = {100: {0: 100, 1: 101, 2: complex_value}, 200: {0: 200}, 300: {1: 301}}
        series = make_series(agent_type, agent_id=0, data=data)
        result = DataTransformerSplit().extract_agent_data([series], agent_type)
        result_0_simple = result[None].xs(0, level=INDEX[0])
        for time, columns in data.items():
            row = result_0_simple.xs(time)
            for column_index, value in columns.items():
                if column_index < 2:
                    assert row[column_index] == value
        result_complex_0 = result["Complex"].xs(0, level=INDEX[0])
        slice_time_agent = result_complex_0.xs(100, level=INDEX[1])
        for value in complex_value:
            slice_sub1 = slice_time_agent.xs(value[0][0], level="Sub1")
            assert slice_sub1.loc[value[0][1]][0] == str(value[1])
