import pandas as pd
from fameprotobuf.Services_pb2 import Output
from pytest import fixture
from mockito import mock, when, unstub, verify

from fameprotobuf.DataStorage_pb2 import DataStorage
from fameio.source.results.agent_type import AgentTypeLog
from fameio.source.results.data_transformer import DataTransformer
from fameio.source.results.output_dao import OutputDAO


@fixture
def mock_log(request):
    mocked_agent_type = mock(AgentTypeLog)
    when(mocked_agent_type).update_agents(...)
    request.addfinalizer(lambda: unstub())
    return mocked_agent_type


def add_series(output: Output, class_name: str) -> Output.Series:
    """Add a new series to given `output` for given class_name with agent_id = 0 and no lines"""
    series = output.series.add()
    series.className = class_name
    series.agentId = 0
    return series


class TestOutputDAO:
    def test_init_works_on_empty_list(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        verify(mock_log).update_agents({})
        assert output_dao._all_series == {}

    def test_init_works_on_none_list(self, mock_log):
        output_dao = OutputDAO(None, mock_log)  # noqa
        verify(mock_log).update_agents({})
        assert output_dao._all_series == {}

    def test_init_update_agent_types_one_data_storage_one_agent_type(self, mock_log):
        data_storage = DataStorage()
        output = data_storage.output
        agent_type = output.agentType.add()
        agent_type.className = "A"

        OutputDAO([data_storage], mock_log)
        verify(mock_log).update_agents({"A": agent_type})

    def test_init_update_agent_types_one_data_storage_multi_agent_types(self, mock_log):
        data_storage = DataStorage()
        output = data_storage.output
        agent_type_a = output.agentType.add()
        agent_type_b = output.agentType.add()

        agent_type_a.className = "A"
        agent_type_b.className = "B"
        OutputDAO([data_storage], mock_log)
        verify(mock_log).update_agents({"A": agent_type_a, "B": agent_type_b})

    def test_init_update_agent_types_multi_data_storage_multi_agent_types(self, mock_log):
        data_storage_1 = DataStorage()
        output = data_storage_1.output
        agent_type_a = output.agentType.add()
        agent_type_a.className = "A"

        data_storage_2 = DataStorage()
        output = data_storage_2.output
        agent_type_b = output.agentType.add()
        agent_type_b.className = "B"
        OutputDAO([data_storage_1, data_storage_2], mock_log)
        verify(mock_log).update_agents({"A": agent_type_a, "B": agent_type_b})

    def test_init_stores_series_one(self, mock_log):
        data_storage = DataStorage()
        output = data_storage.output
        series = add_series(output, "A")

        result = OutputDAO([data_storage], mock_log)
        assert series in result._all_series["A"]

    def test_init_stores_series_multiple_classes_one_data_storage(self, mock_log):
        data_storage = DataStorage()
        output = data_storage.output
        series_a = add_series(output, "A")
        series_b = add_series(output, "B")

        result = OutputDAO([data_storage], mock_log)
        assert series_a in result._all_series["A"]
        assert series_b in result._all_series["B"]

    def test_init_stores_series_multiple_series_one_data_storage(self, mock_log):
        data_storage = DataStorage()
        output = data_storage.output
        series_one = add_series(output, "A")
        series_two = add_series(output, "A")

        result = OutputDAO([data_storage], mock_log)
        assert series_one in result._all_series["A"]
        assert series_two in result._all_series["A"]

    def test_init_stores_series_mixed_data_storage(self, mock_log):
        data_storage_with_series = DataStorage()
        output = data_storage_with_series.output
        series_one = add_series(output, "A")
        data_storage_no_series = DataStorage()

        result = OutputDAO([data_storage_with_series, data_storage_no_series], mock_log)
        assert series_one in result._all_series["A"]

    def test_init_stores_series_multiple_series_multiple_data_storage(self, mock_log):
        data_storage_one = DataStorage()
        output = data_storage_one.output
        series_one_a = add_series(output, "A")
        series_one_b = add_series(output, "B")

        data_storage_two = DataStorage()
        output = data_storage_two.output
        series_two_a = add_series(output, "A")
        series_two_b = add_series(output, "B")

        result = OutputDAO([data_storage_one, data_storage_two], mock_log)
        assert series_one_a in result._all_series["A"]
        assert series_two_a in result._all_series["A"]
        assert series_one_b in result._all_series["B"]
        assert series_two_b in result._all_series["B"]

    def test_get_sorted_agents_to_extract_none_requested_returns_empty_iterable(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        when(mock_log).is_requested(...).thenReturn(False)
        output_dao._all_series = {"A": [1], "B": [2, 2], "C": [3, 2, 1]}
        result = [n for n in output_dao.get_sorted_agents_to_extract()]
        assert result == []

    def test_get_sorted_agents_to_extract_all_requested_returns_in_order(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        when(mock_log).is_requested(...).thenReturn(True)
        output_dao._all_series = {"A": [None], "B": [None, None, None], "C": [None, None]}
        result = [n for n in output_dao.get_sorted_agents_to_extract()]
        assert result == ["A", "C", "B"]

    def test_get_sorted_agents_to_extract_some_requested_returns_in_order(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        output_dao._all_series = {"A": [None], "B": [None, None, None], "C": [None, None]}
        when(mock_log).is_requested(...).thenReturn(True)
        when(mock_log).is_requested("B").thenReturn(False)
        result = [n for n in output_dao.get_sorted_agents_to_extract()]
        assert result == ["A", "C"]

    def test_get_agent_data_missing_calls_data_transformer_empty(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        mocked_data_frame = mock(pd.DataFrame)
        mocked_data_transformer = mock(DataTransformer)
        when(mock_log).get_agent_type("MissingData").thenReturn(mock(Output.AgentType))
        when(mocked_data_transformer).extract_agent_data([], ...).thenReturn(mocked_data_frame)
        result = output_dao.get_agent_data("MissingData", mocked_data_transformer)
        assert result == mocked_data_frame

    def test_get_agent_data_removes_data_from_all_series(self, mock_log):
        output_dao = OutputDAO([], mock_log)
        mocked_data_transformer = mock(DataTransformer)
        when(mocked_data_transformer).extract_agent_data(..., ...).thenReturn(None)
        when(mock_log).get_agent_type(...).thenReturn(mock(Output.AgentType))
        output_dao._all_series = {"A": [1, 2, 3], "B": [4, 5]}
        output_dao.get_agent_data("A", mocked_data_transformer)
        assert "A" not in output_dao._all_series.keys()
        assert "B" in output_dao._all_series.keys()
